import React from "react";
import "./initial.css";

export default function BattleOption(props) {
    return (
        <button className="battleBox" id={props.id} onClick={props.onClick}>
            {props.option}
        </button>
    )
};