import React from "react";
import "./initial.css";
import BattleOption from "./BattleOption"

export default function IntroPage(props) {

    const handleClick = (e)  => {
        props.setBattleChoice(e.target.id)
    }

    const battleOptions = props.battleOptions.map(el =>
        <BattleOption key={el} id={el} onClick={handleClick} option={el}/>
    );
    
    return (
        <div className="intro">
            <div className="introTitle">
                <h1>SUN OF YORK</h1>
                <h2>Tactical Combat During the Wars Of The Roses</h2>
                <p>Brief intro about the game.</p>
            </div>
            <div className="introPage">
                <h4>Choose one of the three options below to start playing:</h4>
                <div className="option">
                {battleOptions}
                </div>
            </div>
        </div>
    )
};
