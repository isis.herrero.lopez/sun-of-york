import React from "react";
import "./battle.css";

export default function FieldSection(props) {
    const name = props.id.split(/(?=[A-Z])/).join(" ");
    return(
        <div className={props.className} id={props.id}>
            <p>{name}</p>
        </div>
    )
}