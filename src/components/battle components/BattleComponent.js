import React, {useReducer} from "react";
import "./battle.css";
import Battlefield from "./Battlefield";
import PlayerSide from "./playerSide/PlayerSide";
import SettingComponent from "./minorComponents/SettingComponent";
import SequenceComponent from "./minorComponents/SequenceComponent";
import InitialPopup from "./popUps/InitialPopup";

//reducer & initial state
const reducer = (state, action) => {
    switch (action.type) {
        case "change_conditions": 
            return {...state, conditions: action.conditions};
        case "change_initiative":
            return {...state, initiative: action.initiative, turn: `1. ${action.initiative}`};
        case "change_yorkPassword":
            return {...state, yorkPassword: action.password};
        case "change_lancasterPassword":
            return {...state, lancasterPassword: action.password};        
        case "change_yorkDeck":
            return {...state, yorkDeck: action.yorkDeck};
        case "change_lancasterDeck":
            return {...state, lancasterDeck: action.lancasterDeck};
        case "change_readyStatus":
            return {...state, readyStatus: action.readyStatus};
        case "change_yorkHand":
            return {...state, yorkDeck: action.yorkDeck, yorkHand: action.yorkHand};
        case "change_lancasterHand":
             return {...state, lancasterDeck: action.lancasterDeck, lancasterHand: action.lancasterHand}; 
        default: 
            return state;
    }
};

const initialState = {
    conditions: "pending",
    initiative: "pending",
    turn: "pending",
    yorkDeckQuantity: 110,
    yorkDeck: [],
    yorkPassword: "",
    yorkHand: [],
    lancasterDeckQuantity: 110,
    lancasterDeck: [],
    lancasterPassword: "",
    lancasterHand : [],
    currentStage: "initial",
    readyStatus: false
};


//main
export default function BattleComponent(props) {
    if (props.battleChoice !== "Random") {
        console.log(props.selectedBattle);
        //change yorkDeckQuantity & lancasterDeckQuantity 
    }

    const [state, dispatch] = useReducer(reducer, initialState);
    const {conditions, initiative, turn, yorkDeckQuantity, yorkDeck, 
        yorkHand, yorkPassword, lancasterDeckQuantity, lancasterDeck, 
        lancasterHand, lancasterPassword, currentStage, readyStatus} 
        = state;

//set conditions
    const changeConditions = (newConditions) => {
        dispatch({type: "change_conditions", conditions: newConditions});
    }
//set initiative
    const getInitiative = (yorkDieValue, lancasterDieValue) => {
        if (yorkDieValue > lancasterDieValue) {
            dispatch({type: "change_initiative", initiative: "York"});
        } else if (lancasterDieValue > yorkDieValue) {
            dispatch({type: "change_initiative", initiative: "Lancaster"});
        }
    }
//set decks (shuffle)
    const getCards = (faction) => {
        let deckQuantity;
        if (faction === "york") {
            deckQuantity = yorkDeckQuantity;
        } else if (faction === "lancaster") {
            deckQuantity = lancasterDeckQuantity;
        }
        
        const cardNumbers = new Array(deckQuantity).fill(0);
        for (let i = 1; i < (deckQuantity + 1); i++) {
            cardNumbers[i - 1] = i;
        }

        const fillingDeck = [];
        while (fillingDeck.length < deckQuantity) {
            const pickedCard = Math.floor(Math.random() * deckQuantity + 1);
            const cardAvailable = cardNumbers.filter(el => el === pickedCard);
            if (cardAvailable.length !== 0) {
                const cardIndex = cardNumbers.findIndex(el => el === pickedCard);
                fillingDeck.push(pickedCard);
                cardNumbers.splice(cardIndex, 1);
            }
        }

        if (fillingDeck.length === deckQuantity) {
            if (faction === "york") {
                dispatch({type: "change_yorkDeck", yorkDeck: fillingDeck});
            } else if (faction === "lancaster") {
                dispatch({type: "change_lancasterDeck", lancasterDeck: fillingDeck});
            }
        }
    }

//register the passwords
    const savePassword = (faction, password) => {
        if (faction ==="York") {
            dispatch({type: "change_yorkPassword", password: password});
        } else  if (faction === "Lancaster") {
            dispatch({type: "change_lancasterPassword", password: password});
        }
    }

//give the first hand
    const dealHands = () => {       
        if (readyStatus === false) {
            dispatch({type: "change_readyStatus", readyStatus: true});  
        };

        if (yorkDeck.length === yorkDeckQuantity) {
            const yorkHandHere = yorkDeck.slice(0, 16);
            const updatedYorkDeck = yorkDeck.slice(16);
            dispatch({type: "change_yorkHand", yorkDeck: updatedYorkDeck, yorkHand: yorkHandHere});
        } else {
            console.log("new hand for york");
        }

        if (lancasterDeck.length === lancasterDeckQuantity) {
            const lancasterHandHere = lancasterDeck.slice(0, 16);
            const updatedLancasterDeck = lancasterDeck.slice(16);
            dispatch({type: "change_lancasterHand", lancasterDeck: updatedLancasterDeck, lancasterHand: lancasterHandHere});
        } else {
            console.log("new hand for lancaster");
        }
    }

    return (
        <div className="gralBoard">
            <PlayerSide id="york" deck={yorkDeck} hand={yorkHand} turn={turn}/>
            <div className="centralSection">
                <SettingComponent conditions={conditions} initiative={initiative} turn={turn} />
                <Battlefield />
                <SequenceComponent currentStage={currentStage} />
            </div>
            <PlayerSide id="lancaster" deck={lancasterDeck} hand={lancasterHand} turn={turn}/>
            <InitialPopup 
                state={state} 
                changeConditions={changeConditions}
                getInitiative={getInitiative}
                getCards={getCards}
                savePassword={savePassword}
                dealHands={dealHands}
            />
        </div>
    )
};
