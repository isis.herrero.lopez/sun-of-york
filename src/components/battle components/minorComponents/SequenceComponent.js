import React from "react";
import ExtraInfoPopup from "../popUps/ExtraInfoPopup";
import "./minor.css"

export default function SequenceComponent(props) {
    const {currentStage} = props;

    const extraInfoSection = 
        <div className="extraInfo" id="extraInfo">
            <h4>Click here for additional information at any stage</h4>
        </div>;  

    return(
        <div className="sequenceComponent">
            <div className="sequenceBox">
                <h4 className="h4Sequence">Sequence of Play:</h4>
                <p className="stages" id="initial">Initial setting</p>
                <p className="stages" id="morale">1. Morale checks</p>
                <p className="stages" id="combat">2. Combat</p>
                <p className="stages" id="movement">3. Movement</p>
                <p className="stages" id="discard">4. Discard <br/>(and new cards)</p>
            </div>
            <ExtraInfoPopup 
                trigger={extraInfoSection}
                currentStage={currentStage}
            />
        </div>
    )
};
