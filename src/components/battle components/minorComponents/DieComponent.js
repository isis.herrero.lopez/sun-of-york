import React from "react";

export default function DieComponent(props) {
    let dieValue = props.number;

    const handleClick = (id) => {
        //add here die images and change them according to props
        props.onClick(id);
    }

    return(
        <button className="die" id={props.id} onClick={(e) => handleClick(e.target.id)} value={dieValue}>{dieValue}
        </button>
    )
}