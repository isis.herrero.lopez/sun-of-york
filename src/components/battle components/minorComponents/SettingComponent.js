import React from "react";
import "./minor.css"

export default function SettingComponent(props) {
    return(
        <div className="setting">
            <div className="settingBox">
                <h4>Weather Conditions:</h4>
                <p>{props.conditions}</p>
                <h4 className="h4Setting">Initiative:</h4>
                <p>{props.initiative}</p>
                <h4 className="h4Setting">Turn:</h4>
                <p>{props.turn}</p>
            </div>
        </div>
    )
} 