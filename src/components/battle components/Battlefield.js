import React from "react";
import "./battle.css";
import FieldSection from "./FieldSection";


export default function Battlefield() {
    const fieldAreas = ["YorkRightFlank", "YorkRightRear", "YorkCentralRear", "YorkLeftRear", "YorkLeftFlank",
    "Middle1", "MiddleCentral", "Middle3",
    "LancasterLeftFlank", "LancasterRightFlank", "LancasterLeftRear", "LancasterCentralRear", "LancasterRightRear"];

    const printAreas = fieldAreas.map(el => {
        if (el.includes("Flank")) {
            return <FieldSection className="flank" key={el} id={el} />
        } else if (el.includes("Central") && el.includes("Middle")) {
            return <FieldSection className="centralMiddle" key={el} id={el} />
        } else if (el.includes("Central")) {
            return <FieldSection className="central" key={el} id={el} />
        } else if (el.includes("Middle")) {
            return <FieldSection className="middle" key={el} id={el} />
        } else {
            return <FieldSection className="rear" key={el} id={el} />
        }
    });

    return(
        <div className="battleField">
            {printAreas}
        </div>
    )
}