import React, {useState} from "react";
import Popup from "reactjs-popup";
import Good from "../../imgs/Good.jpg";
import Fog  from "../../imgs/Fog.jpg";
import Heat from "../../imgs/Heat.jpg";
import Winter  from "../../imgs/Winter.jpg";
import DieComponent from "../minorComponents/DieComponent";
import "./popups.css";

//popUp style (find how to put on css)
const initialStyle = {
    backgroundColor: "#bb864e",
    border: "1px solid brown",
    width: "60%",
    height: "50%",
    color: "white",
    textAlign: "center",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between"
}


export default function InitialPopup(props) {
    const [open, setOpen] = useState(true);
    const [writtenPassword, setPassword] = useState("");
    const {conditions, initiative, yorkDeckQuantity,
    yorkDeck, lancasterDeckQuantity, lancasterDeck, 
    yorkPassword, lancasterPassword, readyStatus} = props.state;

    const weathers = [
        {weather: "Good", modifications: "No changes."},
        {weather: "Fog", modifications: "Reduce missile units' long range fire ability by one (to a minimum of one)."},
        {weather: "Heat", modifications: "All orders are increades by one."},
        {weather: "Winter", modifications: "All units add one to morale rolls. Reduce all missile units' long-range fire ability by one (possibly to zero)."}
    ]

    //conditions infoDisplay
    const weatherBtns = weathers.map(el => {
        const alt = el.weather + " token";
        let img;
        if (el.weather === "Good") {
            img = Good;
        } else if (el.weather === "Fog") {
            img = Fog;
        } else if (el.weather === "Heat") {
            img = Heat;
        } else if (el.weather === "Winter") {
            img = Winter;
        }

        return <div className="conditions" key={el.weather} id={el.weather} 
            onClick={() => weatherClick(el.weather)}>
                <img src={img} alt={alt} className="weatherImg"></img>
                <p>Modifications: </p>
                <p>{el.modifications}</p>
        </div> 
    });

    const weatherClick = (weather) => {
        props.changeConditions(weather);
        /*const selectedWeather = document.getElementById(weather);
        selectedWeather.className = "selectedConditions";

        setTimeout(() => {
            props.changeConditions(weather)
        }, 1500);*/
    }

    //initiative infoDisplay
    const [yorkDieValue, setYorkDieValue] = useState(1);
    const [lancasterDieValue, setLancasterDieValue] = useState(1);
    const [yorkThrow, setYorkThrow] = useState(false);
    const [lancasterThrow, setLancasterThrow] = useState(false);

    //shuffle deck infoDisplay
    const handleClick = () => {
        props.getCards("york");
        props.getCards("lancaster");
    }

    //lastClick
    const handleLastClick = () => {
        setOpen(false);
        props.dealHands();
    }

//step by step of the popup
    let infoDisplayed;
    
    if (conditions === "pending") {
        infoDisplayed = <div className="infoDisplayed">
            <h3>Select the weather conditions: </h3>
            <div className="weathers">
                {weatherBtns}
            </div>
        </div>;
    } else if (initiative === "pending") {
        const throwDie = (id) => {
            const result = Math.floor(Math.random() * 6 + 1);
            if (id === "yorkDie") {
                setYorkDieValue(result);
                setYorkThrow(true);
            } else if (id === "lancasterDie") {
                setLancasterDieValue(result);
                setLancasterThrow(true);
            }
        }

        let extraInfo;
        if (yorkThrow === true && lancasterThrow === true) {
            if (yorkDieValue === lancasterDieValue) {
                extraInfo = <h3>Same result! Throw again!</h3>
                setTimeout(() => {
                    setYorkThrow(false);
                    setLancasterThrow(false);
                }, 2000);
            } else {
                if (yorkDieValue > lancasterDieValue) {
                    extraInfo = <h3>York gets the initiative!</h3>
                } else if (yorkDieValue < lancasterDieValue) {
                    extraInfo = <h3>Lancaster gets the initiative!</h3>
                }
                setTimeout(() => {
                    props.getInitiative(yorkDieValue, lancasterDieValue);
                }, 1500);
            }
        }

        infoDisplayed = <div className="infoDisplayed">
            <h3>Let's determine who starts playing. Click each your die!</h3>
            <div className="diesSection">
                <DieComponent id="yorkDie" onClick={(id) => throwDie(id)} 
                    number={yorkDieValue} />
                <DieComponent id="lancasterDie" onClick={(id) => throwDie(id)}
                    number={lancasterDieValue} />
            </div>
            {extraInfo}
        </div>;
    } else if (yorkDeck.length !== yorkDeckQuantity && lancasterDeck !== lancasterDeckQuantity) {
        infoDisplayed = <div>
            <h3>The conditions are {conditions} and {initiative} got the initiative.</h3>
            <h3>Now, let's shuffle your cards:</h3>
            <button type="submit" className="decks" onClick={handleClick}>Shuffle</button>
        </div>;

    } else if (yorkPassword === "" || lancasterPassword === "") {
        let factionPassword;
        let secondPlayer;
        let secondPlayerPassword;
        if (initiative === "York") {
            factionPassword = yorkPassword;
            secondPlayer = "Lancaster";
            secondPlayerPassword = lancasterPassword;
        } else if (initiative === "Lancaster") {
            factionPassword = lancasterPassword;
            secondPlayer = "York";
            secondPlayerPassword = yorkPassword;
        }

        if (factionPassword === "") {
            infoDisplayed = <div>
                <p>Please, {initiative} player, write a password to protect your hand:</p>
                <input onChange={e => setPassword(e.target.value)} value={writtenPassword}></input>
                <button type="submit" onClick={() => {
                    props.savePassword(initiative, writtenPassword); 
                    setPassword("");
                    }}>Save my password!</button>
            </div>;
        } else if (secondPlayerPassword === "") {
            infoDisplayed = <div>
                <p>Please, {secondPlayer} player, write a password to protect your hand:</p>
                <input onChange={e => setPassword(e.target.value)} value={writtenPassword}></input>
                <button type="submit" onClick={() => props.savePassword(secondPlayer, writtenPassword)}>Save my password!</button>
            </div>;
        }
    } else if (readyStatus === false){
        infoDisplayed = <div>
            <h3>Final settings:</h3>
            <h4>Weather conditions: {conditions}</h4>
            <h4>Initiative: {initiative}</h4>
            <h4>Card decks: both decks are ready!</h4>

            <h2>Let's start playing!</h2>
             <h2>{initiative} starts by deploying his troops!</h2>
            <button onClick={handleLastClick}>Start!</button>
        </div>;
    }

    if (open === true) {
        return (
            <Popup modal position="center center"
            open={open} closeOnDocumentClick className="initialPopup" contentStyle={initialStyle}>
                <div className="section">
                    <h1>Before starting playing, ...</h1>
                    {infoDisplayed}
                </div>
            </Popup>
        )
    } else {
        return (
            <Popup modal position="center center"
            open={open} closeOnDocumentClick className="initialPopup" contentStyle={initialStyle}>
                <div className="section">
                    <h1>Before starting playing, ...</h1>
                    {infoDisplayed}
                </div>
            </Popup>
        )
    }
};
