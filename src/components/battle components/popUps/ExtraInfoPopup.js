import React from "react";
import Popup from "reactjs-popup";

export default function ExtraInfoPopup(props) {
    return(
        <Popup modal closeOnDocumentClick position="center center" id="extrainfoPopup"
        trigger={props.trigger}>
            <p>{props.currentStage}</p>
        </Popup>

    )

}