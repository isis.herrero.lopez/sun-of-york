import React from "react";
import "./player.css";

export default function CardBox(props) {
    let cardsLeft;
    if (props.size) {
        cardsLeft = props.size.length;
    }

    return(
        <div className="cardBox" id={props.side + props.name} name={props.name}>
            <p>{props.name}</p>
            <p>{cardsLeft}</p>
        </div>
    )
}
