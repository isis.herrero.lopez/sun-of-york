import React from "react";
import CardBox from "./CardBox";
import "./player.css";

export default function PlayerSide(props) {
    
    
/*useEffect(() => {
        //const deactivateSide = document.getElementsByClassName("activePlayerSection")[0];
        //deactivateSide.ClassName = "playerSide";
        const active = props.turn.slice(3);
        console.log("section" + active);
        const side = document.getElementById("section" + active);
        console.log(side);
        side.className = "activePlayerSide";
    });
*/

    if (props.id === "lancaster") {
        return(
            <div id="sectionLancaster" className="playerSection" >
                <CardBox side={props.id} name="Deck" size={props.deck}/>
                <div className="handSection">
                    <CardBox side={props.id} name="Hand" size={props.hand} />
                </div>
                <div className="extraSection">
                    <CardBox side={props.id} name="ExtraCard" />
                </div>
                <CardBox side={props.id} name="UsedDeck"/>

            </div>
        )
    } else {
        return(
            <div id="sectionYork" className="playerSection">
                <CardBox side={props.id} name="UsedDeck" />
                <div className="extraSection">
                    <CardBox side={props.id} name="ExtraCard" />
                </div>
                <div className="handSection">
                    <CardBox side={props.id} name="Hand" size={props.hand}/>
                </div>
                <CardBox side={props.id} name="Deck" size={props.deck}/>
            </div>
        )
    }

}