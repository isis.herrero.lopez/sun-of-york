import React, {useState} from "react";
import IntroPage from "./components/initial/IntroPage";
import BattleComponent from "./components/battle components/BattleComponent";
import ListComponent from "./components/initial/ListComponent";
import TournamentComponent from "./components/initial/TournamentComponent";

function App() {
  const battleOptions = ["Random", "Scenario", "Tournament"];
  const [battleChoice, setBattleChoice] = useState(0);
  const [selectedBattle, setSelectedBattle] = useState(0);

  if (battleChoice === 0) {
    return <IntroPage 
    battleOptions={battleOptions} 
    setBattleChoice={setBattleChoice}
    />
  }

  if (battleChoice === "Random") {
    console.log("random");
    return <BattleComponent 
    battleChoice={battleChoice}
    />
  } else if (battleChoice === "Scenario") {
    console.log("scenario");
    return <ListComponent setSelectedBattle={setSelectedBattle} />
  } else if (battleChoice === "Tournament") {
    console.log("tournament");
    return <TournamentComponent />
  }

  if (battleChoice === "Scenario" && selectedBattle !== 0) {
    console.log("scenario, battle no. ", selectedBattle);
    return <BattleComponent battleChoice={battleChoice} selectedBattle={selectedBattle} />
  }
}

export default App;
